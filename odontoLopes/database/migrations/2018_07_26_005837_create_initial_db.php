<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInitialDb extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patients', function (Blueprint $table) {
            $table->Increments('id');
            $table->String('name');
            $table->String('email')->nullable();
            $table->string('telephone')->nullable();
            $table->Timestamps();
            $table->SoftDeletes();
        });


        Schema::create('appointments', function (Blueprint $table) {
            $table->Increments('id');
            $table->dateTime('appointment_date')->nullable()->default(null);
            $table->enum('procedure', ['maintenance', 'molding', 'separator', 'dental_band','superior', 'inferior', 'others']);
            $table->enum('status', ['scheduled', 'done', 'not_done'])->default('scheduled');
            $table->UnsignedInteger('patient_id');
            $table->foreign('patient_id')->references('id')->on('patients');
            $table->Timestamps();
            $table->SoftDeletes();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('appointments', function (Blueprint $table) {
            $table->dropForeign(['patient_id']);
        });

        Schema::dropIfExists('appointments');
        Schema::dropIfExists('patients');
    }
}
