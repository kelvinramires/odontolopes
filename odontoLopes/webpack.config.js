'use strict';
var path = require('path'),
    webpack = require('webpack');

module.exports = {
    cache: true,
    entry: {
        site: path.resolve(__dirname, 'resources', 'assets',  'scripts', 'main.js')
        // manager: path.resolve(__dirname, 'resources', 'assets',  'scripts', 'manager.js')
    },
    output: {
        path: path.resolve(__dirname, 'public', 'assets'),
        filename: 'scripts/[name].js',
        publicPath: './assets/'
    },
    resolve: {
        alias: {
            'site': path.resolve(__dirname,  'resources', 'assets', 'scripts'),
            'scope': path.resolve(__dirname,  'resources', 'assets',  'scripts', 'scope')
        }
    },
    devtool: 'eval',
    plugins: [
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery'
        })
    ],
    module: {
        loaders: [
            { test: /\.json$/, loader: 'json-loader' }
        ]
    }
};
