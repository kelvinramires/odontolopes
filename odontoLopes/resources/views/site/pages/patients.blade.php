@extends('site.layout')

@section('content')

    <div data-current="patients"></div>

    <h1 class="page__title">Pacientes</h1>

    <section id="patient-list">
        
        <form class="patient__filter">
            <input type="search" class="form__input search" placeholder="Filtrar pacientes por nome">
        </form>

        <ul class="patient__list list">

            <div class="loading"></div>

        </ul>

        <ul class="patient__pagination pagination"></ul>

    </section>

@endsection