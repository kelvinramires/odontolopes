@extends('site.layout')

@section('content')

    <div data-current="new_patient"></div>

    <h1 class="page__title">Cadastrar paciente</h1>

    <form class="form" id="new-patient-data">

        <label class="form__label">
            Nome completo <span class="form__required">(*)</span>
            <input type="text" name="name" placeholder="Ex.: José da Silva" class="form__input" required>
        </label>

        <label class="form__label">
            E-mail
            <input type="email" name="email" placeholder="nome@exemplo.com.br" class="form__input">
        </label>

        <label class="form__label">
            Telefone
            <input type="text" name="telephone" placeholder="(00) 00000-0000" class="form__input phone">
        </label>

        <p class="form__note">Campos obrigatórios (*)</p>

        <button type="button" class="form__button" id="new-patient">Cadastrar</button>

    </form>

@endsection