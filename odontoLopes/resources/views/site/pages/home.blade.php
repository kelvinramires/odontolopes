@extends('site.layout')

@section('content')

    <div data-current="home"></div>

    <section class="card">
        <h2 class="card__title">Consultas do dia</h2>
        <ul class="card__list appointments" id="daily_appointments">
            <div class="loading"></div>
        </ul>
        <a href="{{ route('site.appointments') }}" title="" class="card__more"><i class="fas fa-plus-circle"></i> Ver todas</a>
    </section>

@endsection