@extends('site.layout')

@section('content')

    <div data-current="new_appointment"></div>

    <h1 class="page__title">Agendar consulta</h1>

    <form class="form" id="new-appointment-data">

        <label class="form__label">
            Data  <span class="form__required">(*)</span>
            <input type="date" placeholder="DD/MM/AAAA" min="{{ Carbon\Carbon::now()->format('d/m/Y') }}" class="form__input search" required>
        </label>

        <label class="form__label">
            Hora  <span class="form__required">(*)</span>
            <input type="hour" placeholder="00:00" class="form__input time" required>
        </label>

        <input type="hidden" name="appointment_date" class="input__datetime">

        <label class="form__label">
            Paciente <span class="form__required">(*)</span>
            <select name="patient_id" class="form__input patient_id">
                <option value="">Selecione</option>
                @foreach($patients as $patient)
                    <option value="{{ $patient['id'] }}">{{ $patient['name'] }}</option>
                @endforeach
            </select>
        </label>

        <label class="form__label">
            Procedimento <span class="form__required">(*)</span>
            <select name="procedure" class="form__input procedure">
                <option value="">Selecione</option>
                <option value="maintenance">Manutenção</option>
                <option value="molding">Moldagem</option>
                <option value="separator">Separador</option>
                <option value="dental_band">Banda</option>
                <option value="superior">Colagem Superior</option>
                <option value="inferior">Colagem Inferior</option>
                <option value="others">Outros</option>
            </select>
        </label>

        <input type="hidden" name="status" value="scheduled">

        <p class="form__note">Campos obrigatórios (*)</p>

        <button type="button" class="form__button" id="new-appointment">Cadastrar</button>

    </form>

@endsection