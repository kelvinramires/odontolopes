@extends('site.layout')

@section('content')

    <div data-current="appointments"></div>

    <h1 class="page__title">Consultas</h1>

    <ul class="card__list appointments" id="appointments">
        <div class="loading"></div>
    </ul>

    <div class="pagination"></div>

@endsection