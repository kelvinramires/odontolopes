<section class="utbutton__wrapper">
    <ul class="utbutton__list">
        <li class="utbutton__list--item">
            <a href="{{ route('site.newPatient') }}" title="" class="utbutton__list--link"><i class="fas fa-user-plus"></i></a>
        </li>
        <li class="utbutton__list--item">
            <a href="{{ route('site.newAppointment') }}" title="" class="utbutton__list--link"><i class="fas fa-calendar-plus"></i></a>
        </li>
    </ul>
    <a href="javascript:;" title="Menu de contexto" class="utbutton"><i class="fas fa-plus"></i></a>
</section>