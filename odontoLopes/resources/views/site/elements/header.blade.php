<header class="header">

    <div class="header__logo">
        <a href="{{ route('site.home') }}" title="OdontoLopes"><img src="./assets/images/favicon.png" alt="Odontolopes"></a>
    </div>

</header>