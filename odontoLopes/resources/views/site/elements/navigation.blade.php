<a href="javascript:;" title="Clique para abrir/fechar o menu" class="nav__button"></a>

<div class="nav__wrapper">

    <nav class="nav">

        <a href="{{ route('site.home') }}" title="" class="nav__link"><i class="fas fa-home"></i> Painel</a>

        <a href="{{ route('site.patients') }}" title="" class="nav__link"><i class="fas fa-id-card-alt"></i> Pacientes</a>

        <a href="{{ route('site.appointments') }}" title="" class="nav__link"><i class="fas fa-calendar-alt"></i> Consultas</a>

    </nav>

</div>