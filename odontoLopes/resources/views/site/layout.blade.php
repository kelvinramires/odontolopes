<!doctype html>
<html class="root" lang="pt_BR" dir="ltr">
    <head>
        @php
            $title = !empty($title) ? $title : 'Odontolopes';
            $description = !empty($description) ? $description : 'Sistema de gerenciamento de atendimentos odontológicos';
            $keywords = !empty($keywords) ? $keywords : 'Odontolopes';
        @endphp
        <meta name="robots" content="noindex,nofollow">
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, user-scalable=no">
		<meta name="apple-mobile-web-app-capable" content="yes">

        <title>{{ $title }}</title>
        <meta name="description" content="{{ $description }}">
        <meta name="keywords" content="{{ $keywords }}"/>
        <meta name="theme-color" content="#EC3887">

        <!-- Standard favicon -->

        <link rel="shortcut icon" type="image/x-icon" href="{{ asset('/assets/images/favicon.png') }}">
        <link rel="icon" type="image/x-icon" href="{{ asset('/assets/images/favicon.png') }}">

        <!-- Styles -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="./assets/styles/fonts.css" />
        <link rel="stylesheet" type="text/css" href="./assets/styles/site.css" />
    </head>

    <body>

        <main id="main">
            <div id="barba-wrapper">
                <div class="barba-container" id="content">
                    @include('site.elements.header')
                    @include('site.elements.navigation')
                    <div class="site__content">
                        @yield('content')
                    </div>
                    @yield('modals')
                    @include('site.elements.utility-button')
                    <section class="message__wrapper"></section>
                </div>
            </div>
            <div class="global-loading">
                <div class="loading"></div>
            </div>
        </main>
        
        <script>
            var ODONTOLOPES = window.ODONTOLOPES || {};
            ODONTOLOPES.base_url = "{{ url('/') }}";
        </script>

        <script src="./assets/scripts/site.js"></script>

        <div id="scripts">@section('scripts') @show</div>

    </body>
</html>
