<!doctype html>
<html class="root" lang="pt_BR" dir="ltr">
    <head>
        <!-- Basics -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
        <base href="{{ url('/') }}/" id="base-id">

        @php
            $title = !empty($title) ? $title : 'Odontolopes';
            $description = !empty($description) ? $description : 'Sistema de gerenciamento de atendimentos odontológicos';
            $keywords = !empty($keywords) ? $keywords : 'Odontolopes';
        @endphp

        <title>{{ $title }}</title>
        <meta name="description" content="{{ $description }}">
        <meta name="keywords" content="{{ $keywords }}"/>

        <!-- Standard favicon -->

        <link rel="shortcut icon" type="image/x-icon" href="{{ asset('/assets/images/favicon.png') }}">
        <link rel="icon" type="image/x-icon" href="{{ asset('/assets/images/favicon.png') }}">

        <!-- Styles -->
        <link rel="stylesheet" type="text/css" href="./assets/styles/fonts.css" />
        <link rel="stylesheet" type="text/css" href="./assets/styles/site.css" />
    </head>

    <body>
        
        <main id="main">
            <div id="barba-wrapper">
                <div class="barba-container" id="content">
                    @yield('content')
                </div>
            </div>
            <div class="global-loading">
                <div class="loading"></div>
            </div>
        </main>

        <script>
            var ODONTOLOPES = window.ODONTOLOPES || {};
            ODONTOLOPES.base_url = "{{ url('/') }}";
        </script>

        <script src="./assets/scripts/site.js"></script>
        
        <div id="scripts">@section('scripts') @show</div>

    </body>
</html>
