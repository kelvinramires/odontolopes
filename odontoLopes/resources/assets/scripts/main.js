var $ = require('jquery'),
    Barba = require('barba.js'),
    magnific = require('magnific-popup'),
    scope = require('scope'),
    mask = require('jquery-mask-plugin'),
    showMessage = require('./libs/messages.js'),
    ModalListener = require('./listeners/modal.js'),
    CommonListener = require('./listeners/common.js');

$(function(){

    scope.statuses = {
        maintenance:'Manutenção',
        molding:'Moldagem',
        separator:'Separador',
        dental_band:'Banda',
        superior:'Colagem Superior',
        inferior:'Colagem Inferior',
        others:'Outros'
    }

    Barba.Dispatcher.on('newPageReady', function(currentStatus, oldStatus, container) {

        CommonListener(container);
        ModalListener(container);

    });

    Barba.Pjax.getTransition = require('./transition.js');
    Barba.Pjax.init();

});
