require('magnific-popup/dist/jquery.magnific-popup.js');

var ModalListener = function(container){
    if ($('[data-modal]', container).length > 0) {
        $(container).find('a[data-modal],button[data-modal]').each(function () {
            var button = $(this), type = $(this).data('modal');
            var target = $();

            try {
                target = $($(this).attr('href'));
            } catch(e) {}

            var options = {
                tClose: 'Fechar (Esc)',
                tLoading: 'Carregando...',
                gallery: {
                    tPrev: 'Anterior (Seta esquerda)',
                    tNext: 'Próximo (Seta direita)',
                    tCounter: '%curr% of %total%'
                },
                image: {
                    tError: 'A <a href="%url%">imagem</a> não pode ser carregada.'
                },
                ajax: {
                    tError: '<a href="%url%">A página</a> não pode ser carregada.'
                },

                type: type,
                mainClass: 'mfp-animate ' + target.attr('data-modal-class'),
                removalDelay: 160,
                preloader: false,
                fixedContentPos: true,
                iframe: {
                    patterns: {
                        youtube: {
                            src: '//www.youtube.com/embed/%id%?html5=1&autoplay=1&rel=0&wmode=opaque&modestbranding=1&enablejsapi=1&iv_load_policy=3&showinfo=0&fs=0&controls=2&origin=' + window.location.origin
                        }
                    }
                },
                items: {
                    src: $(this).attr('href') || $(this).data('modal-href')
                },
                callbacks: {
                    open: function () {
                        ModalListener(this.container);
                    },
                    beforeClose: function () {
                    },
                    close: function () {
                        button.trigger('modalClose');

                        if (button.parents('.slick-initialized').length > 0) {
                            button.parents('.slick-initialized').slick('slickPlay');
                        }
                    }
                }
            };

            $(this).magnificPopup(options);
        });
    }
}

module.exports = ModalListener;
