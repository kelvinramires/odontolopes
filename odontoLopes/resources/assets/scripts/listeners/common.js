var scope = require('scope'),
    Barba = require('barba.js'),
    home = require('../pages/home.js'),
    patients = require('../pages/patients.js'),
    newPatient = require('../pages/new-patient.js'),
    appointments = require('../pages/appointments.js'),
    newAppointment = require('../pages/new-appointment.js'),
    jqueryDatepicker = require('jquery-datepicker');

module.exports = function(container) {

    // Check what page is active
    var currentPage = $(container).find('[data-current]').data('current');
    switch (currentPage){
        case 'home':
        home();
        break;

        case 'patients':
        patients();
        break;

        case 'new_patient':
        newPatient();
        break;

        case 'appointments':
        appointments();
        break;

        case 'new_appointment':
        newAppointment();
        break;
    }

    jqueryDatepicker($);
    $('input[data-datepicker]').each(function(e,el){
        $(el).datepicker({
            minDate: new Date()
        });
    });
    $.datepicker.regional[ "pt-BR" ] = {
        closeText: "Fechar",
        prevText: "&#x3C;Anterior",
        nextText: "Próximo&#x3E;",
        currentText: "Hoje",
        monthNames: [ "Janeiro","Fevereiro","Março","Abril","Maio","Junho",
        "Julho","Agosto","Setembro","Outubro","Novembro","Dezembro" ],
        monthNamesShort: [ "Jan","Fev","Mar","Abr","Mai","Jun",
        "Jul","Ago","Set","Out","Nov","Dez" ],
        dayNames: [
            "Domingo",
            "Segunda-feira",
            "Terça-feira",
            "Quarta-feira",
            "Quinta-feira",
            "Sexta-feira",
            "Sábado"
        ],
        dayNamesShort: [ "Dom","Seg","Ter","Qua","Qui","Sex","Sáb" ],
        dayNamesMin: [ "Dom","Seg","Ter","Qua","Qui","Sex","Sáb" ],
        weekHeader: "Sm",
        dateFormat: "dd/mm/yy",
        firstDay: 0,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ""
    };
    $.datepicker.setDefaults( $.datepicker.regional[ "pt-BR" ] );

    // Navigation
    $('.nav__button').click(function(e){
        e.preventDefault();

        $(this).toggleClass('active');
        $('.nav__wrapper').toggleClass('opened');

    });

    // Utility Button
    $('.utbutton').click(function(e){
        e.preventDefault();
        
        // Torna o botão ativo e mostra os links
        $(this).toggleClass('active blocked');
        $('.utbutton__list').toggle();

        // Seleciona os links e calcula o tamanho do array
        var elem = $('.utbutton__list--item');
        var total = elem.length - 1;
        elem.removeClass('visible');
        
        // Previne o clique duplo antes do fim da animação
        var totalTime = total*200;
        setTimeout(function(){
            $('.utbutton').removeClass('blocked');
        },totalTime);
        
        $.each(elem,function(e,el){
        
            setTimeout(function(){
                $(elem[total - e]).addClass('visible');
            }, e*50);
        
        });
        
    });

}
