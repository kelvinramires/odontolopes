var $ = require('jquery'),
    Barba = require('barba.js');

var pageTransition = Barba.BaseTransition.extend({
    start: function() {
        Promise
            .all([this.newContainerLoading, this.fadeOut()])
            .then(this.fadeIn.bind(this));
    },
    fadeOut: function() {
        $('.global-loading').fadeIn();
        return $(this.oldContainer).animate({ opacity: 0 }).promise();
    },
    fadeIn: function() {
        var self = this;
        var $el = $(this.newContainer);

        
        $(this.oldContainer).hide();
    
        $el.css({
            visibility : 'visible',
            opacity : 0
        });
    
        $el.animate({ opacity: 1 }, 400, function() {
            $('.global-loading').fadeOut();
            self.done();
        });
    }
});

module.exports = function(){
    return pageTransition;
};