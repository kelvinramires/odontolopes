var $ = require('jquery'),
    Barba = require('barba.js'),
    scope = require('scope'),
    showMessage = require('../libs/messages.js');

module.exports = function(container) {

    Barba.Dispatcher.on('transitionCompleted', function(){

        var buildPatientsList = function(url){
            if(url){
                serviceURL = url;
            } else {
                serviceURL = scope.base_url + '/api/appointments';
            }
            $.ajax({
                url: serviceURL,
                method:'GET',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json'
            }).done(function(response){

                // Consultas
                var appointments = '',
                    numOfInvalids = 0;
                $.each(response.appointments.data,function(i,appointment){

                    if(appointment.patients != null){
                        var datetime = appointment.appointment_date.split(' '),
                            date = datetime[0].split('-'),
                            hour = datetime[1].split(':');

                        appointments += '<li class="card__list--item appointments__item"><p class="appointments__item--date">';
                        appointments += '<span class="appointments__item--hour">'+hour[0]+':'+hour[1]+'</span><span class="appointments__item--day">'+date[2]+'/'+date[1]+'</span></p>';
                        appointments += '<div class="appointments__item--info"><p class="appointments__item--patient">'+appointment.patients.name+'</p><p class="appointments__item--procedure">'+scope.statuses[appointment.procedure]+'</p></div>';
                        if(appointment.status == 'not_done'){
                            appointments += '<div class="appointments__item--status appointments__item--status-failed"><i class="fas fa-calendar-times"></i></div>';
                        } else if(appointment.status == 'done'){
                            appointments += '<div class="appointments__item--status appointments__item--status-checked"><i class="fas fa-calendar-check"></i></div>';
                        } else {
                            appointments += '<div class="appointments__item--status appointments__item--status-scheduled"><i class="fas fa-calendar-alt"></i></div>';
                        }
                        appointments += '</li>';
                    } else {
                        appointment = '';
                        numOfInvalids++;
                    }

                });

                $('#appointments').html(appointments);

                // Paciente excluído
                if(numOfInvalids > 0){
                    showMessage(numOfInvalids+' consulta(s) com paciente excluído foi omitida','alert');
                }

                if(appointments == ''){
                    appointments = '<li class="card__list--item empty">Nenhuma consulta agendada.<br> Aproveite o dia! :)</li>';
                }

                // Paginação
                var pagination = '';
                if(response.appointments.prev_page_url != null){
                    pagination += '<a href="javascript:;" data-url="'+response.appointments.prev_page_url+'" class="pagination__link"><i class="fas fa-arrow-circle-left"></i> Anterior</a>';
                }
                pagination += '<p class="pagination__status"><strong>'+response.appointments.from+'</strong> a <strong>'+response.appointments.to+'</strong> de <strong>'+response.appointments.total+'</strong></p>';
                if(response.appointments.next_page_url != null){
                    pagination += '<a href="javascript:;" data-url="'+response.appointments.next_page_url+'" class="pagination__link">Próximo <i class="fas fa-arrow-circle-right"></i></a>';
                }
                
                $('.pagination').html('').html(pagination);

                $('.pagination__link').click(function(e){
                    e.preventDefault();
                    var url = $(this).data('url');
                    buildPatientsList(url);
                });

            });
        }

        buildPatientsList();

    });

};