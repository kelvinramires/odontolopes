var $ = require('jquery'),
    Barba = require('barba.js'),
    scope = require('scope'),
    showMessage = require('../libs/messages.js');

module.exports = function(container) {

    Barba.Dispatcher.on('transitionCompleted', function(){

        var SPMaskBehavior = function (val) {
            return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
        },
            spOptions = {
            onKeyPress: function(val, e, field, options) {
                field.mask(SPMaskBehavior.apply({}, arguments), options);
            }
        };
            
        $('.phone').mask(SPMaskBehavior, spOptions);
        
        // Form submit
        $('#new-patient').unbind('click').click(function(e){
            e.preventDefault();

            var rawdata = $('#new-patient-data').serialize();

            $.ajax({
                url: scope.base_url + '/api/patients/save?'+rawdata,
                method:'POST',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json'
            }).done(function(response){
                if(response.success == true){
                    showMessage('Paciente cadastrado com sucesso','success');
                    $('.form__input').val('');
                } else {
                    showMessage('Confira os campos!','error');
                }
            });

        });

    });

};