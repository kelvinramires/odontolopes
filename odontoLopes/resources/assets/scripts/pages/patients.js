var $ = require('jquery'),
    Barba = require('barba.js'),
    scope = require('scope'),
    List = require('list.js'),
    showMessage = require('../libs/messages.js');

module.exports = function(container) {

    Barba.Dispatcher.on('transitionCompleted', function(){

        $.ajax({
            url: scope.base_url + '/api/patients',
            method:'GET',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json'
        }).done(function(response){

            // Pacientes
            var patients = '';
            $.each(response.patients,function(i,patient){

                patients += '<li class="patient"><div class="patient__info"><p class="patient__name">'+patient.name+'</p>';
                if(patient.email != null){
                    patients += '<p class="patient__email"><i class="fas fa-envelope"></i> '+patient.email+'</p>';
                }
                if(patient.telephone != null){
                    patients += '<p class="patient__phone"><i class="fas fa-phone"></i> '+patient.telephone+'</p>';
                }
                patients += '</div><div class="patient__actions" data-id="'+patient.id+'"><a href="javascript:;" title="Excluir" class="patient__delete"><i class="fas fa-user-times"></i></a></div></li>';

            });

            $('.patient__list').html(patients);

            var options = {
                valueNames: [
                    'patient__name',
                    'patient__email',
                    'patient__phone',
                ],
                page:10,
                pagination:true        
            };
            
            var mainList = new List('patient-list', options);        

            // Delete
            $('.patient__delete').unbind('click').on('click',function(e){
                e.preventDefault();
                var userID = $(e.currentTarget).parent().data('id');
                
                if(confirm('Tem certeza que deseja excluir esse paciente?')){

                    $.ajax({
                        url: scope.base_url + '/api/patients/remove/'+userID,
                        contentType: 'application/json; charset=utf-8',
                        dataType: 'json'
                    }).done(function(response){
                        if(response.success){
                            buildPatientsList();
                            showMessage('Usuário excluído com sucesso.','success');
                        } else {
                            showMessage('Erro ao excluir usuário','error');
                        }
                    });

                }
                
            });

        });

    });

};