var $ = require('jquery'),
    Barba = require('barba.js'),
    scope = require('scope'),
    showMessage = require('../libs/messages.js');

module.exports = function(container) {

    Barba.Dispatcher.on('transitionCompleted', function(){

        // Field treatments
        $('.form__input.time').mask('00:00');

        // Build datetime
        var datetime = function(){
            var day  = $('.form__input.date').val(),
                time = $('.form__input.time').val(),
                formatted = day.split('-'),
                date = formatted[2]+'/'+formatted[1]+'/'+formatted[0];
            
            $('.input__datetime').val(date+' '+time);
        }
        
        // Form submit
        $('#new-appointment').unbind('click').click(function(e){
            e.preventDefault();

            datetime();
            var rawdata = $('#new-appointment-data').serialize();

            $.ajax({
                url: scope.base_url + '/api/appointments/save?'+rawdata,
                method:'POST',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json'
            }).done(function(response){
                if(response.success == true){
                    showMessage('Consulta agendada com sucesso.','success');
                    $('.form__input').val('');
                } else {
                    showMessage('Confira os campos!','error');
                }
            });

        });

    });

};