var scope = require('scope');

module.exports = function(content,type) {

    // Limpando a área de mensagens
    clearTimeout(hideMessage);
    clearTimeout(removeMessage);
    $('.message').remove();

    // Criando a mensagem
    var type = type ? type : 'default';

    var message = '<div class="message message-'+type+'">'+
                    '<p class="message__text">'+content+'</p>'+
                '</div>';

    $('.message__wrapper').append(message);
    $('.message').fadeIn();

    var hideMessage = setTimeout(function(){
        $('.message').fadeOut();
    },10000);

    var removeMessage = setTimeout(function(){
        $('.message').remove();
    },12000);

}