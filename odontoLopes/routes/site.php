<?php

Route::get('/', 'PagesController@home')->name('home');
Route::get('/pacientes', 'PagesController@patients')->name('patients');
Route::get('/paciente', 'PagesController@patient')->name('patient');
Route::get('/novo-paciente', 'PagesController@newPatient')->name('newPatient');
Route::get('/editar-paciente', 'PagesController@editPatient')->name('editPatient');
Route::get('/consultas', 'PagesController@appointments')->name('appointments');
Route::get('/nova-consulta', 'PagesController@newAppointment')->name('newAppointment');