<?php


Route::prefix('/patients')->name('patients.')->group(function () {
    Route::name('index')->get('/', 'PatientsController@index');
    Route::name('add')->get('/add', 'PatientsController@edit');
    Route::name('edit')->get('/edit/{patientId}', 'PatientsController@edit');
    Route::name('save')->post('/save', 'PatientsController@save');
    Route::name('remove')->get('/remove/{patientId}', 'PatientsController@remove');
});

Route::prefix('/appointments')->name('appointments.')->group(function () {
    Route::name('index')->get('/', 'AppointmentsController@index');
    Route::name('add')->get('/add', 'AppointmentsController@edit');
    Route::name('edit')->get('/edit/{appointmentId}', 'AppointmentsController@edit');

    Route::name('dailyAppointments')->get('/dailyAppointments', 'AppointmentsController@dailyAppointments');
    Route::name('save')->post('/save', 'AppointmentsController@save');
    Route::name('remove')->get('/remove/{appointmentId}', 'AppointmentsController@remove');
});