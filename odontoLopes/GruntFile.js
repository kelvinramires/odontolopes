'use strict';


module.exports = function (grunt) {

    var webpack = require('webpack'),
        webpackConfig = require("./webpack.config.js");
    // Time how long tasks take. Can help when optimizing build times
    require('time-grunt')(grunt);

    // Load grunt tasks automatically
    require('jit-grunt')(grunt);


    // configurable paths
    var config = {
        source: './resources/assets/',
        dist: './public/assets',
        assets: {
            cssmin: {},
            header: {}
        }
    };

    grunt.initConfig({
        config: config,
        watch: {
            sass: {
                files: ['<%= config.source %>/styles/**'],
                tasks: ['sass:watch'],
                options: {
                    atBegin: true
                }
            }
        },
        clean: {
            dist: {
                files: [{
                    dot: true,
                    src: [
                        './public/assets/'
                    ]
                }]
            },
            deploy: {
                files: [{
                    dot: true,
                    src: [
                        './output/'
                    ]
                }]
            }
        },
        webpack: {
            options: webpackConfig,
            build: {
                output: {
                    path: require('path').resolve(__dirname,  './public/assets')
                },
                devtool: 'sourcemap'
                // plugins: webpackConfig.plugins.concat(
                //     new webpack.DefinePlugin({
                //         'process.env': {
                //             'NODE_ENV': JSON.stringify('production')
                //         }
                //     }),
                //     new webpack.optimize.UglifyJsPlugin()
                // )
            },
            watch: {
                progress: true,
                devtool: 'sourcemap',
                failOnError: false,
                watch: true,
                keepalive: true
            }
        },
        sass: {
            options: {
                includePaths: ['./node_modules/'],
                sourceMap: true
            },
            watch: {
                files: {
                    '<%= config.dist %>/styles/site.css': '<%= config.source %>/styles/site.scss',
                    // '<%= config.dist %>/styles/manager.css': '<%= config.source %>/styles/manager.scss',
                    '<%= config.dist %>/styles/fonts.css': '<%= config.source %>/styles/fonts.scss'
                }
            },
            dist: {
                options: {
                    outputStyle: 'compressed'
                },
                files: {
                    '<%= config.dist %>/styles/site.css': '<%= config.source %>/styles/site.scss',
                    // '<%= config.dist %>/styles/manager.css': '<%= config.source %>/styles/manager.scss',
                    '<%= config.dist %>/styles/fonts.css': '<%= config.source %>/styles/fonts.scss'
                }
            }
        },
        autoprefixer:{
            dist:{
                files:{
                    '<%= config.dist %>/styles/site.css':'<%= config.dist %>/styles/site.css'
                    // '<%= config.dist %>/styles/manager.css':'<%= config.dist %>/styles/manager.css'
                }
            }
        },
        // Put files not handled in other tasks here
        copy: {
            dist: {
                files: [
                    {
                        expand: true,
                        dot: true,
                        cwd: '<%= config.source %>',
                        dest: '<%= config.dist %>',
                        src: [
                            '**/*.{ico}',
                            'favicon/**/*',
                            'images/**/*.*',
                            'scripts/**/*.{json}',
                            'scripts/data/*.*',
                            'styles/fonts/**/*.{eot,svg,ttf,woff,woff2}'
                        ]
                    },
                    {
                        expand: true,
                        cwd: '<%= config.source %>/images',
                        src: '**/*.{png,jpg,jpeg,gif,svg,ico}',
                        dest: '<%= config.dist %>/images'
                    }
                ]
            },
            dev: {
                files: [
                    {
                        expand: true,
                        dot: true,
                        cwd: '<%= config.source %>',
                        dest: './public/assets',
                        src: [
                            '**/*.{ico}',
                            'favicon/**/*',
                            'images/**/*.*',
                            'scripts/**/*.{json}',
                            'scripts/data/*.*',
                            'styles/fonts/**/*.{eot,svg,ttf,woff,woff2}'
                        ]
                    }
                ]
            },
            deploy: {
                files: [
                    {
                        expand: true,
                        dot: true,
                        cwd: './',
                        dest: 'output',
                        src: [
                            'app/**/*',
                            'bootstrap/**/*',
                            'config/**/*',
                            'database/**/*',
                            'public/assets/**/*',
                            'public/assets/scripts/data/*.*',
                            'public/.*',
                            'public/*.*',
                            'storage/app/public/images/init/*.*',
                            'resources/views/**/*',
                            'resources/lang/**/*',
                            'resources/assets/scripts/site/data/**/*',
                            'routes/**/*',
                            'storage/**/.gitignore',
                            'composer.*',
                            'artisan',
                            '.htaccess',
                            '.env.servershot',
                            '.env.prod'
                        ]
                    }
                ]
            }
        },
        run: {
            image: {
                exec: 'npm run sync'
            },
            composer: {
                exec: 'cd ./output && composer install --prefer-dist -o'
            }
        },
        concurrent: {
            dev: {
                tasks: ['watch:sass', 'webpack:watch', 'run:image', 'copy:dev'],
                options: {
                    logConcurrentOutput: true
                }
            }
        }
    });

    grunt.registerTask('build', [
        'clean:dist',
        'webpack:build',
        'sass:dist',
        'autoprefixer:dist',
        'copy:dist'
    ]);

    grunt.registerTask('deploy', [
        'clean:deploy',
        'build',
        'copy:deploy',
        'run:composer'
    ]);

    grunt.registerTask('dev', [
        'concurrent:dev'
    ]);

    grunt.registerTask('default', [
        'build'
    ]);
};
