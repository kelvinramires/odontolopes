<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Models\Patient;
use Illuminate\Http\Request;


class PagesController extends Controller
{

    public function home()
    {
        return view('site.pages.home');
    }

    public function patients()
    {
        return view('site.pages.patients');
    }

    public function newPatient()
    {
        return view('site.pages.new-patient');
    }

    public function editPatient()
    {
        return view('site.pages.new-patient');
    }

    public function patient()
    {
        return view('site.pages.profile');
    }

    public function newAppointment()
    {
        return view('site.pages.new-appointment',[
            'patients' => Patient::orderBy('name','ASC')->get()
        ]);
    }

    public function appointments()
    {
        return view('site.pages.appointments');
    }
}
