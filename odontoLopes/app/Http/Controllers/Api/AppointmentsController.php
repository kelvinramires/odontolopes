<?php
/**
 * Created by PhpStorm.
 * User: kelvinramires
 * Date: 25/07/2018
 * Time: 22:29
 */


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Http\Resources\AppointmentResource;
use App\Models\Appointment;
use Carbon\Carbon;
use Illuminate\Http\Request;


class AppointmentsController extends Controller
{

    public function index()
    {
        return [
            'appointments' => Appointment::with('patients')->orderBy('appointment_date', 'ASC')->paginate(15)
        ];

    }

    public function dailyAppointments()
    {
        return [
            'appointments' =>  Appointment::with('patients')
                ->whereDate('appointment_date', Carbon::today())
                ->orderBy('appointment_date', 'ASC')->paginate(15),
        ];
    }

    public function edit($appointmentId = null)
    {
        $appointment = Appointment::findOrNew($appointmentId);

        return  AppointmentResource::make($appointment);
    }

    public function save(Request $request)
    {
        $inputs = $request->all();
        $inputs['appointment_date'] = Carbon::createFromFormat('d/m/Y H:i', $inputs['appointment_date']);
        try {
            $appointment = Appointment::findOrNew($request->input('id', null));
            $appointment->fill($inputs);
            $appointment->save();
            return [
                'appointment' => AppointmentResource::make($appointment),
                'success' => true,
            ];
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error_code' => $e->getCode(),
                'error_msg' => $e->getMessage()
            ];
        }
    }

    public function delete($appointmentId)
    {
        try {
            Appointment::findOrFail($appointmentId)->delete();
            return [
                'success' => true
            ];

        } catch (\Exception $e) {
            return [
                'success' => false,
                'error_code' => $e->getCode(),
                'error_msg' => $e->getMessage()
            ];
        }

    }
}