<?php
/**
 * Created by PhpStorm.
 * User: kelvinramires
 * Date: 25/07/2018
 * Time: 22:29
 */


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Models\Patient;
use Illuminate\Http\Request;


class PatientsController extends Controller
{

    public function index()
    {
        return [
            'patients' => Patient::orderBy('name', 'ASC')->get()
        ];

    }

    public function edit($patientId = null)
    {
        $patient = Patient::findOrNew($patientId);

        return [
            'patient' => $patient
        ];
    }

    public function save(Request $request)
    {
        $inputs = $request->all();
        try {
            $patient = Patient::findOrNew($request->input('id', null));
            $patient->fill($inputs);
            $patient->save();
            return [
                'success' => true,
            ];
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error_code' => $e->getCode(),
                'error_msg' => $e->getMessage()
            ];
        }
    }

    public function remove($patientId)
    {
        try {
            Patient::findOrFail($patientId)->delete();
            return [
                'success' => true
            ];

        } catch (\Exception $e) {
            return [
                'success' => false,
                'error_code' => $e->getCode(),
                'error_msg' => $e->getMessage()
            ];
        }

    }
}