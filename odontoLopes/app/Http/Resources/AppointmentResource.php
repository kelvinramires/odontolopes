<?php
/**
 * Created by PhpStorm.
 * User: kelvinramires
 * Date: 28/07/2018
 * Time: 23:38
 */


namespace App\Http\Resources;
use App\Models\Patient;
use Illuminate\Http\Resources\Json\JsonResource;

class AppointmentResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        $model = $this->resource->toArray();
        $model['patient'] = (empty($this->resource->patients) ? '': $this->resource->patients);
        $model['allPatients'] = Patient::get()->toArray();
        return array_merge($model);
    }
}
