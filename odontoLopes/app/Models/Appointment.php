<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Appointment extends Model
{
    use SoftDeletes;

    public $fillable = [
        'procedure',
        'status',
        'appointment_date',
        'patient_id',
    ];

    public $dates = [
        'appointment_date'
    ];

    protected $appends = ['formatted_status', 'formatted_procedure'];

    private $formattedStatus = [
        'scheduled' => 'Agendado',
        'done' => 'Realizado',
        'not_done' => 'Não Realizado',
    ];

    private $formattedProcedure = [
        'maintenance' => 'Manutenção',
        'molding' => 'Moldagem',
        'separator' => 'Separador',
        'dental_band' => 'Banda',
        'superior' => 'Colagem Superior',
        'inferior' => 'Colagem Inferior',
        'others' => 'Outros',
    ];


    /**
     * Relations
     */

    public function patients()
    {
        return $this->belongsTo(Patient::class, 'patient_id');
    }


    /**
     * Scopes
     */

    /**
     * Custom Attributes
     */
    public function getFormattedStatusAttribute()
    {
        return $this->formattedStatus[$this->status];
    }

    public function getFormattedProcedureAttribute()
    {
        return $this->formattedProcedure[$this->procedure];
    }

    /**
     * Methods
     */
}
