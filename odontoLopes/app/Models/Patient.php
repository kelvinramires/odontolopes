<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Patient extends Model
{
    use SoftDeletes;

    public $fillable = [
        'name',
        'email',
        'telephone',
    ];

    /**
     * Relations
     */

    public function appointments()
    {
        return $this->hasMany(Appointment::class, 'patient_id');
    }
   }
